#include <iostream>
#include "my_lab.h"
using namespace std;

int CopyWord(char* str, char* word)
{
	int k = 0, j = 0;

	while ((str[k] != '\n') && (isspace(str[k])))
	{
		k++;
	}

	if (isalpha(str[k]))
	{
		while ((str[k] != '\n') && (isalpha(str[k])))
		{
			word[j++] = str[k++];
		}
		word[j] = '\0';

		if (strcmp(word, "return") == 1)
			return -1;

		return k;
	}
	else
		return -1;
}

void Check(char* str, char* word, int i, FILE* file2)
{
	int k = i, inw = 0, count = 0;

	while (str[i] != '\n')
	{
		if ((inw == 0) && (isalnum(str[i])))
			count++;

		isalnum(str[i]) == 0 ? inw = 0 : inw = 1;

		if ((count == 0) && (!((str[i] == '*') || (str[i] == ' ') || str[i] == '&')))
			return;

		if ((count == 1) && ((str[i] == ',') || (str[i] == ';') || str[i] == '[') || (str[i] == '='))
		{
			if (str[i] == '[')
			{
				cout << "*";
				fputs("*", file2);
			}
			
			cout << word;
			fputs(word, file2);
			
			for (k; k < i; k++)
			{
				cout << str[k];
				fputc(str[k], file2);
			}

			cout << '\n';
			fputs("\n", file2);

			while (!((str[i] == ',') || (str[i] == ';'))) i++;

			if (str[i] == ',')
			{
				Check(str, word, i + 1, file2);
			}
			return;
		}
		i++;
	}
}

void func(FILE* file)
{
	FILE* file2 = fopen("file2.txt", "wt");
	char *word = new char[50], *str = new char[501];
	int i = 0;

	while (fgets(str, 500, file) != NULL)
	{
		i = CopyWord(str, word);

		if (i != -1)
		{
			Check(str, word, i, file2);
		}
		else
		{
			continue;
		}
	}

	delete[] str;
	delete[] word;
	fclose(file2);
}