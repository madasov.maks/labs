#include <iostream>
#include "header.h"
using namespace std;

Queue::Queue(int s)
{
	size = 0;
	cap = size == 0 ? 1 : s*2;
	que = new int[cap];
}

Queue::Queue(Queue& a)
{
	size = a.size;
	cap = a.cap;
	que = new int[cap];
	for (int i = 0; i < cap; i++)  que[i] = a.que[i];
}

Queue& Queue::operator=(const Queue& a)
{
	if (this == &a)  
		return *this;

	delete[] que;
	size = a.size;
	cap = a.cap;
	que = new int[cap];
	for (int i = 0; i < cap; i++) que[i] = a.que[i];
	return *this;
}

void Queue::set(int i, int value)
{
	if (i < 0 || i > size)
		exit(-1);
	que[i] = value;
}

void Queue::push(int value)
{
	if (size == cap)
	{
		cap *= 2;
		int *tmp = new int[cap];
		for (int i = 0; i <= size; i++)  tmp[i] = que[i];
		delete[] que;
		que = tmp;
	}			
	que[size] = value;
	size++;
}

int Queue::front()
{
	int value = get(0);
	size--;
	for (int i = 0; i < size; i++)
	{
		que[i] = que[i + 1];
	}
	return value;
}

void Queue::print()
{
	for (int i = 0; i < size; i++) cout << que[i] << " ";
	cout << endl;
}

ostream& operator<<(ostream& out, const Queue& a)
{
	for (int i = 0; i < a.size; i++) out << a.que[i] << " ";
	return out;
}

int& Queue::operator[](const int i)
{
	if (i < 0 || i > size)
		exit(-2);
	return que[i];
}