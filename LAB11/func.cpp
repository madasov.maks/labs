#include <iostream>
using namespace std;

long strtolong(const char* str, char** end, int base)
{
	if (base == 1 || base < 0 || base > 36) exit(3);

	int i = 0, neg = 0, a = -1, j = 0, o = 0;
	long value = 0, tmp = 0;

	while (isspace(str[i])) {	
		i++;
	}

	if (str[i] == '-') {
		neg = 1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	
	if ((base == 0 || base == 16) && (str[i] == '0' && (str[i + 1] == 'x' || str[i + 1] == 'X'))) {
		i += 2;
		base = 16;
	}

	if (base == 0)
		base = str[i] == '0' ? 8 : 10;

	while (str[i] != '\n') {
		if (isdigit(str[i]))
			a = str[i] - '0';
		else if (isalpha(str[i]) && base > 10)
			a = isupper(str[i]) ? str[i] - 'A' + 10: str[i] - 'a' + 10;
		else
			break;

		if (a >= base)
			break;

		tmp = value * base;
		if (value != 0 && (tmp / value != base || tmp / base != value))
		{
			while (isdigit(str[i])) i++;
			*end = (char*)&str[i];
			return neg ? 1 -LONG_MAX : LONG_MAX;
		}
		value *= base;
		if ((value + a) < value || (value + a) < a)
		{
			while (isdigit(str[i])) i++;
			*end = (char*)&str[i];
			return neg ? 1 - LONG_MAX : LONG_MAX;
		}
		value += a;

		i++;
		o++;
	}
	if ((a == -1) || (base == 16 && o == 0))
		*end = (char*) &str[0];
	else
		*end = (char*) &str[i];

	if (neg == 1)
		value = -value;

	return value;
}