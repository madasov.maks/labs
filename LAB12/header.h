#pragma once
using namespace std;

class Queue {
private:
	int* que;
	int size;
	int cap;

public:
	Queue(int s = 0);

	Queue(Queue& a);

	Queue& operator=(const Queue& a);

	void push(int value);
	
	int front();

	int getsize() { return size; }

	int get(int i) { return que[i]; }

	void set(int i, int value);

	void print();

	friend ostream& operator<<(ostream& out, const Queue& a);

	int& operator[](const int i);

	~Queue() { delete[] que; }
};