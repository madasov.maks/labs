//17.Создайте класс Queue, хранящий целые числа в виде очереди.
//Реализовать операции push(добавление в конец очереди) и front(взятие элемента из начала очереди).
//Количество элементов очереди не ограничено.
#include <iostream>
#include "header.h"
using namespace std;

int main()
{
	Queue a, b;

	for (int i = 0; i < 10; i++) a.push(i);
	a = a;
	cout << a << endl;
	
	cout << a.front() << endl;
	cout << a << endl;

	b = a;

	cout << b.front() << endl;
	cout << b << endl;

	Queue c(a);
	cout << c << endl;

	Queue g(4);
	g.push(90);
	g.push(15);
	
	cout << g[1] << endl;

	int k = a.getsize();
	cout << a[k] << endl;

	return 0;
}