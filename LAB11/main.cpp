//17.  strtol.
#include <iostream>
#include <ctime>
#include "Header.h"
using namespace std;

void check(char* str, char* End, char* End2)
{
	int a = 0, o = 0, i = 0, k = 0, j = 0;
	for (int y = 0; y < 1000; y++)
	{
		a = rand() % 3 + 1;
		o = rand() % 35 + 2;
		switch (a)
		{
		case 1:
			str[i++] = '0';
			j = rand() % 100000;
			break;
		case 2:
			str[i++] = '0';
			str[i++] = 'x';
			j = rand() % 100000;
			break;
		default:
			j = rand() % 100000;
		}
		for (i; i < 7; i++)
		{
			k = j % 10;
			j /= 10;
			str[i] = k + 48;
		}

		cout << str << endl;

		long value = strtolong(str, &End, o);
		long value2 = strtol(str, &End2, o);

		if (value != value2 || End != End2)
		{
			cout << str << endl;
			cerr << "!" << endl;
			cout << "MY   --- " << value << " : " << End << ";" << endl;
			cout << "ORIG --- " << value2 << " : " << End2 << ";" << endl << endl;
		}
		i = 0;
	}
}

int main()
{
	char str[] = { " -0x111111111111        SimpleText" };
	char* End = NULL;
	char* End2 = NULL;

	srand(time(0));

	//check(str, End, End2);

	for (int u = 0; u < 1; u++) {
		long value = strtolong(str, &End, u);
		long  value2 = strtol(str, &End2, u);
		cout << u << endl;
		cout << "MY   --- " << value << " : " << End << ";" << endl;
		cout << "ORIG --- " << value2 << " : " << End2 << ";" << endl;
		if (value != value2 || End != End2)
			cerr << "!" << endl;
	}

	return 0;
}